package com.epam.rd.java.basic.task8.util;

import java.io.File;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;


/**
 * Util class for document transformation XML.
 * 
 * @author D.Kim
 * 
 */
public class Transformer {

	private Transformer() {
	}

	/**
	 * Save DOM model in XML file (DOM --> XML file).
	 * 
	 * @param document
	 *            DOM document to be saved.
	 * @param xmlFileName
	 *            Output XML file.
	 */
	public static void saveToXML(Document document, String xmlFileName) 
			throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));
		
		// set up transformation
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");			
		
		// run transformation
		t.transform(new DOMSource(document), result);
	}
}