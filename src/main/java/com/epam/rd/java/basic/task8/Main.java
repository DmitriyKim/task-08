package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.util.Sorter;
import ua.nure.Flowers;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		Flowers flowers = domController.getFlowers();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		// sort
		Sorter.sortFlowersByNames(flowers);
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		// save
		DOMController.saveXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(true);
		flowers = saxController.getFls();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		// sort
		Sorter.sortFlowersByAveLenFlw(flowers);
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowers, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		flowers = staxController.getFls();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		// sort
		Sorter.sortFlowersByTemp(flowers);
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowers, outputXmlFile);
	}

}
