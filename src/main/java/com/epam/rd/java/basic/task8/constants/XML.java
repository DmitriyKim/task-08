package com.epam.rd.java.basic.task8.constants;

/**
 * Holds elements and attributes names from the XML schema.
 * @author D.Kim
 *
 */
public enum XML {
	// these are tags
	FLOWERS("flowers"), FLOWER("flower"), NAME("name"), SOIL("soil"),ORIGIN("origin"),
	VISUAL_PARAMETERS("visualParameters"),
		STEM_COLOUR("stemColour"), LEAF_COLOUR("leafColour"), AVE_LEN_FLOWER("aveLenFlower"),
	GROWING_TIPS("growingTips"),
		TEMPERATURE("tempreture"), LIGHTING("lighting"), WATERING("watering"),
	MULTIPLYING("multiplying"),

	// these are attributes
	MEASURE("measure"),LIGHT_REQUIRING("lightRequiring");

	private String value;

	public String value() {
		return value;
	}

	XML(String value) {
		this.value = value.intern();
	}
}
