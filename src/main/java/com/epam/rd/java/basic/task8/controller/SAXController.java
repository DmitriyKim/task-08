package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.Flowers;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 *
 * @author D.Kim
 *
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	/**
	 * Parses XML document.
	 *
	 * @param validate
	 *            If true validate XML document against its XML schema. With
	 *            this parameter it is possible make parser validating.
	 */
	public void parse(boolean validate) throws ParserConfigurationException,
			SAXException, IOException {

		// get sax parser factory
		SAXParserFactory factory = SAXParserFactory.newInstance(
				Constants.CLASS_SAX_PARSER_FACTORY_INTERNAL,
				this.getClass().getClassLoader());

		factory.setNamespaceAware(true);
		if (validate) {
			factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	// ///////////////////////////////////////////////////////////
	// ERROR HANDLER IMPLEMENTATION
	// ///////////////////////////////////////////////////////////

	@Override
	public void error(org.xml.sax.SAXParseException e) throws SAXException {
		throw e; // <-- if XML document not valid just throw exception
	}

	// ///////////////////////////////////////////////////////////
	// CONTENT HANDLER IMPLEMENTATION
	// ///////////////////////////////////////////////////////////

	private String currentElement; // <-- current element name holder

	private Flowers flowers; // <-- main container
	private Flowers.Flower flower;
	private Flowers.Flower.VisualParameters vp;
	private Flowers.Flower.VisualParameters.AveLenFlower alf;
	private Flowers.Flower.GrowingTips gt;
	private Flowers.Flower.GrowingTips.Tempreture t;
	private Flowers.Flower.GrowingTips.Lighting l;
	private Flowers.Flower.GrowingTips.Watering w;

	public Flowers getFls() {
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
							 Attributes attributes) {

		currentElement = localName;

		// WARNING!!!
		// here and below we use '==' operation to compare two INTERNED STRINGS
		if (currentElement == XML.FLOWERS.value()) {
			flowers = new Flowers();
			return;
		}

		if (currentElement == XML.FLOWER.value()) {
			flower = new Flowers.Flower();
			return;
		}

		if (currentElement == XML.VISUAL_PARAMETERS.value()) {
			vp = new Flowers.Flower.VisualParameters();
			return;
		}

		if (currentElement == XML.AVE_LEN_FLOWER.value()) {
			alf = new Flowers.Flower.VisualParameters.AveLenFlower();
			if (attributes.getLength() > 0) {
				alf.setMeasure((attributes.getValue(uri, XML.MEASURE.value())));
			}
		}

		if (currentElement == XML.GROWING_TIPS.value()) {
			gt = new Flowers.Flower.GrowingTips();
			return;
		}

		if (currentElement == XML.TEMPERATURE.value()) {
			t = new Flowers.Flower.GrowingTips.Tempreture();
			if (attributes.getLength() > 0) {
				t.setMeasure((attributes.getValue(uri, XML.MEASURE.value())));
			}
			return;
		}

		if (currentElement == XML.LIGHTING.value()) {
			l = new Flowers.Flower.GrowingTips.Lighting();
			if (attributes.getLength() > 0) {
				l.setLightRequiring(attributes.getValue(XML.LIGHT_REQUIRING.value()));
			}
			return;
		}

		if (currentElement == XML.WATERING.value()) {
			w = new Flowers.Flower.GrowingTips.Watering();
			if (attributes.getLength() > 0) {
				w.setMeasure((attributes.getValue(uri, XML.MEASURE.value())));
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
	{

		String elementText = new String(ch, start, length).trim();

		if (elementText.isEmpty()) // <-- return if content is empty
			return;

		if (currentElement == XML.NAME.value()) {
			flower.setName(elementText);
			return;
		}

		if (currentElement == XML.SOIL.value()) {
			flower.setSoil(elementText);
			return;
		}

		if (currentElement == XML.ORIGIN.value()) {
			flower.setOrigin(elementText);
			return;
		}

		if (currentElement == XML.MULTIPLYING.value()) {
			flower.setMultiplying(elementText);
			return;
		}

		if (currentElement == XML.STEM_COLOUR.value()) {
			vp.setStemColour(elementText);
			return;
		}

		if (currentElement == XML.LEAF_COLOUR.value()) {
			vp.setLeafColour(elementText);
			return;
		}

		if (currentElement == XML.AVE_LEN_FLOWER.value()) {
			alf.setValue(new BigInteger(elementText));
			return;
		}

		if (currentElement == XML.TEMPERATURE.value()) {
			t.setValue(new BigInteger(elementText));
			return;
		}

		if (currentElement == XML.WATERING.value()) {
			w.setValue(new BigInteger(elementText));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
	{

		if (localName == XML.FLOWER.value()) {
			// just add question to container
			flowers.getFlowers().add(flower);
			return;
		}

		if (localName == XML.VISUAL_PARAMETERS.value()) {
			// just add question to container
			flower.setVisualParameters(vp);
			return;
		}

		if (localName == XML.AVE_LEN_FLOWER.value()) {
			// just add question to container
			vp.setAveLenFlower(alf);
			return;
		}

		if (localName == XML.GROWING_TIPS.value()) {
			// just add question to container
			flower.setGrowingTips(gt);
			return;
		}

		if (localName == XML.TEMPERATURE.value()) {
			// just add question to container
			gt.setTempreture(t);
			return;
		}

		if (localName == XML.LIGHTING.value()) {
			// just add question to container
			gt.setLighting(l);
			return;
		}

		if (localName == XML.WATERING.value()) {
			// just add question to container
			gt.setWatering(w);
		}
	}

}