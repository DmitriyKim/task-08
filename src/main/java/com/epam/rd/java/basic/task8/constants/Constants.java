package com.epam.rd.java.basic.task8.constants;

/**
 * Holds application constants.
 * 
 * @author D.Kim
 * 
 */
public final class Constants {

	private Constants() {
	}

	public static final String JAXB_PACKAGE = "ua.nure";

	// your own namespace
	public static final String MY_NS_URI = "http://www.nure.ua";
	public static final String MY_NS_PREFIX = "";
	
	// for schema location
	public static final String SCHEMA_LOCATION_URI =
			"http://www.nure.ua input.xsd ";
	public static final String SCHEMA_LOCATION_ATTR_NAME = "schemaLocation";
	public static final String SCHEMA_LOCATION_ATTR_FQN = "xsi:schemaLocation";
	public static final String XSI_SPACE_PREFIX = "xsi";

	// validation features
	public static final String FEATURE_TURN_VALIDATION_ON =
			"http://xml.org/sax/features/validation";
	public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON =
			"http://apache.org/xml/features/validation/schema";
	public static final String JAXP_SCHEMA_LANGUAGE =
		    "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	public static final String JAXP_SCHEMA_SOURCE =
			"http://java.sun.com/xml/jaxp/properties/schemaSource";

	// full qualified names of classes
	public static final String CLASS_XERCES_SAX_PARSER =
			"org.apache.xerces.parsers.SAXParser";
	public static final String CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL =
			"com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl";
	public static final String CLASS_SAX_PARSER_FACTORY_INTERNAL =
			"com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl";
	public static final String CLASS_XML_SCHEMA_FACTORY_INTERNAL =
			"com.sun.org.apache.xerces.internal.jaxp.validation.XMLSchemaFactory";
}