package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.util.Transformer;
import ua.nure.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigInteger;


/**
 * Controller for DOM parser.
 *
 * @author D.Kim
 *
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	private Flowers flowers; // <-- container

	public Flowers getFlowers() {
		return flowers;
	}

	/**
	 * Parses XML document.
	 *
	 * @param validate
	 *            If true validate XML document against its XML schema.
	 */
	public void parse(boolean validate) throws ParserConfigurationException,
			SAXException, IOException {

		// obtain DOM parser

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
				Constants.CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL,
				this.getClass().getClassLoader());

		// set properties for Factory
		dbf.setNamespaceAware(true); // <-- XML document has namespace
		if (validate) { // <-- make parser validating
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);

		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e; // <-- throw exception if XML document is NOT valid
			}
		});

		Document document = db.parse(xmlFileName); // <-- parse XML document
		Element root = document.getDocumentElement(); // <-- get root element

		// create container
		flowers = new Flowers();

		NodeList flowerNodes = root.getElementsByTagName(XML.FLOWER.value());

		for (int j = 0; j < flowerNodes.getLength(); j++) {
			Flowers.Flower flower = getFlower(flowerNodes.item(j));
			flowers.getFlowers().add(flower); // <-- add flower to container
		}
	}
	/**
	 * Extracts flower object from the question XML node.
	 *
	 * @param fNode
	 *            Flower node.
	 * @return Flower object.
	 */
	private Flowers.Flower getFlower(Node fNode) {
		Flowers.Flower flower = new Flowers.Flower();
		Element fElement = (Element) fNode;


		// process name text
		Node nNode = fElement.getElementsByTagName(XML.NAME.value()).item(0);
		// set name text
		flower.setName(nNode.getTextContent());
		Node sNode = fElement.getElementsByTagName(XML.SOIL.value()).item(0);
		flower.setSoil(sNode.getTextContent());
		Node oNode = fElement.getElementsByTagName(XML.ORIGIN.value()).item(0);
		flower.setOrigin(oNode.getTextContent());
		Node vpNode = fElement.getElementsByTagName(XML.VISUAL_PARAMETERS.value()).item(0);
		flower.setVisualParameters(getVisualParameters(vpNode));
		Node gtNode = fElement.getElementsByTagName(XML.GROWING_TIPS.value()).item(0);
		flower.setGrowingTips(getGrowingTips(gtNode));
		Node mNode = fElement.getElementsByTagName(XML.MULTIPLYING.value()).item(0);
		flower.setMultiplying(mNode.getTextContent());

		return flower;
	}

	private Flowers.Flower.VisualParameters getVisualParameters(Node vpNode) {
		Flowers.Flower.VisualParameters vp = new Flowers.Flower.VisualParameters();
		Element vpElement = (Element) vpNode;

		Node scNode = vpElement.getElementsByTagName(XML.STEM_COLOUR.value()).item(0);
		vp.setStemColour(scNode.getTextContent());
		Node lcNode = vpElement.getElementsByTagName(XML.LEAF_COLOUR.value()).item(0);
		vp.setLeafColour(lcNode.getTextContent());
		Node alfNode = vpElement.getElementsByTagName(XML.AVE_LEN_FLOWER.value()).item(0);
		vp.setAveLenFlower(getAveLenFlower(alfNode));

		return vp;
	}

	private Flowers.Flower.VisualParameters.AveLenFlower getAveLenFlower(Node alfNode) {
		Flowers.Flower.VisualParameters.AveLenFlower alf = new Flowers.Flower.VisualParameters.AveLenFlower();
		Element alfElement = (Element) alfNode;

		// process measure
		String measure = alfElement.getAttribute(XML.MEASURE.value());
		alf.setMeasure(measure); // <-- set measure
		// process value
		BigInteger value = new BigInteger(alfElement.getTextContent());
		alf.setValue(value); // <-- set value

		return alf;
	}

	private Flowers.Flower.GrowingTips getGrowingTips(Node gtNode) {
		Flowers.Flower.GrowingTips gt = new Flowers.Flower.GrowingTips();
		Element gtElement = (Element) gtNode;

		Node tNode = gtElement.getElementsByTagName(XML.TEMPERATURE.value()).item(0);
		gt.setTempreture(getTemperature(tNode));
		Node lNode = gtElement.getElementsByTagName(XML.LIGHTING.value()).item(0);
		gt.setLighting(getLighting(lNode));
		Node wNode = gtElement.getElementsByTagName(XML.WATERING.value()).item(0);
		gt.setWatering(getWatering(wNode));

		return gt;
	}

	private Flowers.Flower.GrowingTips.Tempreture getTemperature(Node tNode) {
		Flowers.Flower.GrowingTips.Tempreture t = new Flowers.Flower.GrowingTips.Tempreture();
		Element tElement = (Element) tNode;

		String measure = tElement.getAttribute(XML.MEASURE.value());
		t.setMeasure(measure);
		BigInteger value = new BigInteger(tElement.getTextContent());
		t.setValue(value);

		return t;
	}

	private Flowers.Flower.GrowingTips.Lighting getLighting(Node lNode) {
		Flowers.Flower.GrowingTips.Lighting l = new Flowers.Flower.GrowingTips.Lighting();
		Element lElement = (Element) lNode;

		String lightRequiring = lElement.getAttribute(XML.LIGHT_REQUIRING.value());
		l.setLightRequiring(lightRequiring);

		return l;
	}

	private Flowers.Flower.GrowingTips.Watering getWatering(Node wNode) {
		Flowers.Flower.GrowingTips.Watering w = new Flowers.Flower.GrowingTips.Watering();
		Element wElement = (Element) wNode;

		String measure = wElement.getAttribute(XML.MEASURE.value());
		w.setMeasure(measure);
		BigInteger value = new BigInteger(wElement.getTextContent());
		w.setValue(value);

		return w;
	}

	public static void saveXML(Flowers flowers, String outputXmlFile)
			throws ParserConfigurationException, TransformerException {

		// obtain DOM parser
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(Constants.CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL,
				DOMController.class.getClass().getClassLoader());
		// LET SONAR BE HAPPY
		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_DTD, "");
		dbf.setAttribute(javax.xml.XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

		// set properties for Factory
		dbf.setNamespaceAware(true); // <-- XML document has namespace

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// this is the root element
		Element fsElement = document.createElementNS(
				Constants.MY_NS_URI, XML.FLOWERS.value());

		// set schema location
		fsElement.setAttributeNS(
				XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
				Constants.SCHEMA_LOCATION_ATTR_FQN,
				Constants.SCHEMA_LOCATION_URI);

		document.appendChild(fsElement);

		// add flower elements
		for (Flowers.Flower flower : flowers.getFlowers()) {
			// add flower
			Element fElement = document.createElement(XML.FLOWER.value());
			fsElement.appendChild(fElement);

			// add flower name
			Element nElement = document.createElement(XML.NAME.value());
			nElement.setTextContent(flower.getName());
			fElement.appendChild(nElement);

			// add flower soil
			Element sElement = document.createElement(XML.SOIL.value());
			sElement.setTextContent(flower.getSoil());
			fElement.appendChild(sElement);


			// add flower origin
			Element oElement = document.createElement(XML.ORIGIN.value());
			oElement.setTextContent(flower.getOrigin());
			fElement.appendChild(oElement);

			// add flower visualParameters
			Element vpElement = document.createElement(XML.VISUAL_PARAMETERS.value());
			fElement.appendChild(vpElement);

			Element stElement = document.createElement(XML.STEM_COLOUR.value());
			stElement.setTextContent(flower.getVisualParameters().getStemColour());
			vpElement.appendChild(stElement);

			Element lcElement = document.createElement(XML.LEAF_COLOUR.value());
			lcElement.setTextContent(flower.getVisualParameters().getLeafColour());
			vpElement.appendChild(lcElement);

			Element alfElement = document.createElement(XML.AVE_LEN_FLOWER.value());
			alfElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
			// set attribute
			alfElement.setAttribute(XML.MEASURE.value(),
					flower.getVisualParameters().getAveLenFlower().getMeasure());
			vpElement.appendChild(alfElement);


			// add flower growingTips
			Element gtElement = document.createElement(XML.GROWING_TIPS.value());
			fElement.appendChild(gtElement);

			Element tElement = document.createElement(XML.TEMPERATURE.value());
			tElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
			// set attribute
			tElement.setAttribute(XML.MEASURE.value(),
					flower.getGrowingTips().getTempreture().getMeasure());
			gtElement.appendChild(tElement);

			Element lElement = document.createElement(XML.LIGHTING.value());
			// set attribute
			lElement.setAttribute(XML.LIGHT_REQUIRING.value(),
					flower.getGrowingTips().getLighting().getLightRequiring());
			gtElement.appendChild(lElement);

			Element wElement = document.createElement(XML.WATERING.value());
			wElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
			// set attribute
			wElement.setAttribute(XML.MEASURE.value(),
					flower.getGrowingTips().getWatering().getMeasure());
			gtElement.appendChild(wElement);

			// add flower multiplying
			Element mElement = document.createElement(XML.MULTIPLYING.value());
			mElement.setTextContent(flower.getMultiplying());
			fElement.appendChild(mElement);
		}
		Transformer.saveToXML(document, outputXmlFile);
	}
}
