package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import org.xml.sax.helpers.DefaultHandler;
import ua.nure.Flowers;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 *
 * @author D.Kim
 *
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	private String currentElement; // <-- current element name holder

	private Flowers flowers; // <-- main container
	private Flowers.Flower flower;
	private Flowers.Flower.VisualParameters vp;
	private Flowers.Flower.VisualParameters.AveLenFlower alf;
	private Flowers.Flower.GrowingTips gt;
	private Flowers.Flower.GrowingTips.Tempreture t;
	private Flowers.Flower.GrowingTips.Lighting l;
	private Flowers.Flower.GrowingTips.Watering w;

	public Flowers getFls() {
		return flowers;
	}

	/**
	 * Parses XML document with StAX API. There is no validation during the
	 * parsing.
	 */
	public void parse() throws XMLStreamException {

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(
				new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			// skip any empty content
			if (event.isCharacters() && event.asCharacters().isWhiteSpace())
				continue;

			// handler for start tags
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				// WARNING!!!
				// here and below we use '==' operation to compare two INTERNED STRINGS
				if (currentElement == XML.FLOWERS.value()) {
					flowers = new Flowers();
					continue;
				}

				if (currentElement == XML.FLOWER.value()) {
					flower = new Flowers.Flower();
					continue;
				}

				if (currentElement == XML.VISUAL_PARAMETERS.value()) {
					vp = new Flowers.Flower.VisualParameters();
					continue;
				}

				if (currentElement == XML.AVE_LEN_FLOWER.value()) {
					alf = new Flowers.Flower.VisualParameters.AveLenFlower();
					Attribute attribute = startElement.getAttributeByName(
							new QName(XML.MEASURE.value()));
					if (attribute != null) {
						alf.setMeasure(attribute.getValue());
					}
				}

				if (currentElement == XML.GROWING_TIPS.value()) {
					gt = new Flowers.Flower.GrowingTips();
					continue;
				}

				if (currentElement == XML.TEMPERATURE.value()) {
					t = new Flowers.Flower.GrowingTips.Tempreture();
					Attribute attribute = startElement.getAttributeByName(
							new QName(XML.MEASURE.value()));
					if (attribute != null) {
						t.setMeasure(attribute.getValue());
					}
					continue;
				}

				if (currentElement == XML.LIGHTING.value()) {
					l = new Flowers.Flower.GrowingTips.Lighting();
					Attribute attribute = startElement.getAttributeByName(
							new QName(XML.LIGHT_REQUIRING.value()));
					if (attribute != null) {
						l.setLightRequiring(attribute.getValue());
					}
					continue;
				}

				if (currentElement == XML.WATERING.value()) {
					w = new Flowers.Flower.GrowingTips.Watering();
					Attribute attribute = startElement.getAttributeByName(
							new QName(XML.MEASURE.value()));
					if (attribute != null) {
						w.setMeasure(attribute.getValue());
					}
				}
			}

			// handler for contents
			if (event.isCharacters()) {
				Characters characters = event.asCharacters();

				if (currentElement == XML.NAME.value()) {
					flower.setName(characters.getData());
					continue;
				}

				if (currentElement == XML.SOIL.value()) {
					flower.setSoil(characters.getData());
					continue;
				}

				if (currentElement == XML.ORIGIN.value()) {
					flower.setOrigin(characters.getData());
					continue;
				}

				if (currentElement == XML.MULTIPLYING.value()) {
					flower.setMultiplying(characters.getData());
					continue;
				}

				if (currentElement == XML.STEM_COLOUR.value()) {
					vp.setStemColour(characters.getData());
					continue;
				}

				if (currentElement == XML.LEAF_COLOUR.value()) {
					vp.setLeafColour(characters.getData());
					continue;
				}

				if (currentElement == XML.AVE_LEN_FLOWER.value()) {
					alf.setValue(new BigInteger(characters.getData()));
					continue;
				}

				if (currentElement == XML.TEMPERATURE.value()) {
					t.setValue(new BigInteger(characters.getData()));
					continue;
				}

				if (currentElement == XML.WATERING.value()) {
					w.setValue(new BigInteger(characters.getData()));
					continue;
				}

			}

			// handler for end tags
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if (localName == XML.FLOWER.value()) {
					flowers.getFlowers().add(flower);
					continue;
				}

				if (localName == XML.VISUAL_PARAMETERS.value()) {
					// just add question to container
					flower.setVisualParameters(vp);
					continue;
				}

				if (localName == XML.AVE_LEN_FLOWER.value()) {
					// just add question to container
					vp.setAveLenFlower(alf);
					continue;
				}

				if (localName == XML.GROWING_TIPS.value()) {
					// just add question to container
					flower.setGrowingTips(gt);
					continue;
				}

				if (localName == XML.TEMPERATURE.value()) {
					// just add question to container
					gt.setTempreture(t);
					continue;
				}

				if (localName == XML.LIGHTING.value()) {
					// just add question to container
					gt.setLighting(l);
					continue;
				}

				if (localName == XML.WATERING.value()) {
					// just add question to container
					gt.setWatering(w);
				}

			}
		}
		reader.close();
	}
}