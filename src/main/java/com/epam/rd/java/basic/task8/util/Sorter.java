package com.epam.rd.java.basic.task8.util;

import ua.nure.Flowers;

import java.util.Collections;
import java.util.Comparator;



/**
 * Contains static methods for sorting.
 * 
 * @author D.Kim
 * 
 */
public class Sorter {
	////////////////////////////////////////////////////////////
	// these are comparators
	////////////////////////////////////////////////////////////

	private Sorter() {
	}

	/**
	 * Sorts flowers by name
	 */
	public static final Comparator<Flowers.Flower> sortFlowersByName = new Comparator<Flowers.Flower>() {
		@Override
		public int compare(Flowers.Flower o1, Flowers.Flower o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};


	/**
	 * Sorts flowers by aveLenFlower.
	 */
	public static final Comparator<Flowers.Flower> sortFlowersByAveLenFlower =
			new Comparator<Flowers.Flower>() {
				@Override
				public int compare(Flowers.Flower o1, Flowers.Flower o2) {
					return (o1.getVisualParameters().getAveLenFlower().getValue().subtract(o2.getVisualParameters().getAveLenFlower().getValue())).intValue();
				}
	};

	/**
	 * Sorts flowers by Tempreture.
	 */
	public static final Comparator<Flowers.Flower> sortFlowersByTempreture =
			new Comparator<Flowers.Flower>() {
				@Override
				public int compare(Flowers.Flower o1, Flowers.Flower o2) {
					return (o1.getGrowingTips().getTempreture().getValue().subtract(o2.getGrowingTips().getTempreture().getValue())).intValue();
				}
			};

	////////////////////////////////////////////////////////////
	// these methods take Flowers object and sort it
	// with according comparator
	////////////////////////////////////////////////////////////
	
	public static final void sortFlowersByNames(Flowers flowers) {
		Collections.sort(flowers.getFlowers(), sortFlowersByName);
	}

	public static final void sortFlowersByAveLenFlw(Flowers flowers) {
		Collections.sort(flowers.getFlowers(), sortFlowersByAveLenFlower);

	}

	public static final void sortFlowersByTemp(Flowers flowers) {
		Collections.sort(flowers.getFlowers(), sortFlowersByTempreture);
	}
}